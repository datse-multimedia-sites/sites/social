# Default Follows

We'd set things up so that the default follows was unset, thinking that would
have a person follow the admin account (we currently only have one admin),
and we found that when we checked to see our users, there was a new user which
did not show a new follow of the operator account, so we've set that to include
both that, and our own account makes sense.

So now we have the following accounts as follows:

* [operator](https://social.datsemultimedia.com/@operator) - The main admin 
  this posts the information about the instance.
* [JigmeDatse](https://social.datsemultimedia.com/@JigmeDatse) - My personal
  account, and a moderator.  

More may be added in the future.  