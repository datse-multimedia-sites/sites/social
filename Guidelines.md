# Guidelines

Here we will have a bit of an idea of how we handle any issues that come up.

## Local Users

Local users who wish to engage in a decision about what to do about some issue
are prioritized over federated users.

We support our local users.  

That said.  Local users who violate the spirt of our 
[code of conduct](https://gitlab.com/datse-multimedia-sites/sites/social/blob/master/Code%20of%20Conduct.md) 
will not be supported over remote users who have not.  

## Remote Users

We support remote users to send reports, or contact our 
[staff](https://gitlab.com/datse-multimedia-sites/sites/social/blob/master/Staff.md)
about any issues they have with users or content orriginating from our site.  

## Staff

The staff may make mistakes, feel free to consider that possibility.

## Changes to this or other information

This may change, and probably will over time, to indicate the changes in the
way that we will handle things.  As we handle more issues we see, we will try
to explain them, and also come up with some way to have a "general guidelines"
which would help to understand why that decision would be made that way.

If you see something that isn't working, let us know.  