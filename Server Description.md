A while ago we started hosting mastodon on our own, and managed to get it up 
and running, but decided that it was too much trouble, and too expensive to 
run.

It seems that now we've decided to look at masto.host as a hosting option, as 
they specialize in hosting mastodon.

This is really mostly setup so that we can move away from relying on admin/mod 
staff that we have on our current instance who seem to be, "as long as the 
letter of the rules is followed, 'they did nothing wrong.'"

We feel that when moderating, we need to understand our users first, and if 
they have bothered to send a request, be sure that we are serving our users, 
and finding ways to serve them.

Our rules are there for a reason, so that we have guidelines to understand what
might be decided, in advance not as rules which we go through a list to 
determine whether or not a given user/instance is a problem.

We aim to serve our users, and to provide a space where our users are 
comfortable.  These first are the users who are admin/mod staff here.

We hope to be able to pay the admin/mod staff in time, though don't totally 
expect that to be an option for some time.