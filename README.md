# Social

[Social site](https://social.datsemultimedia.com/) currently hosted on 
[masto.host](https://masto.host/) as a 
[mastodon](https://github.com/tootsuite/mastodon) instance.  