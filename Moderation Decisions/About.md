# About

This is where (the directory) we will have explanations about what decisions we
have made around moderation.

## User Decisions

These are decisions which get made about users of the fediverse, including
local users.

Unless a decision is made about a user who seems to be making massive impact
on either our isntance, or the federation as a whole, we will simply refer to
them with an indicator as to who they are, "internally" to us (only accessible
through the moderation interface) such as:

User: @user1@instance1 

These will remain consistent, provided the username/instance does not change.

@user1@instance1 is in no way related to @user1@instance2

It's based on the interaction on that instance...

## Instance decisions

These will indicate what instance they are from.  

## Location of these decision lists

For now, we are undecided as to whether or not to leave these decisions linked
from this, or create a separate summary, which links to the further discussion
about the decision made.  