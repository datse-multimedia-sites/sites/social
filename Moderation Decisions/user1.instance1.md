# @ user1@instance1

## 2019-05-31 21:01 

This is a long standing bot which follows broadly, and has the following in
their profile:

> If you don't feel confortable with me following you, tell me: unfollow and 
> I'll do it :)
> 
> If you want me to follow you, just tell me follow !
> 
> If you want automatic follow for new users on your instance and you are an 
> instance admin, contact me !

The wording of this suggests that the bot will not follow users unless they
have requested being followed.

This bot, has followed my account (user/moderator account, not admin account) 
with no action taken on our part.  

We are currently silencing the account, and considering what to do with the 
instance, as the admin of the instance runs the bot.

Have contacted the owner...  