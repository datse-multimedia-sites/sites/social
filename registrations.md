# Registrations

Registrations are open, or have been recently closed.

If the registrations are closed they are available from:

* Adminstrators
* Moderators
* Users

If you have been told that registrations are closed, yet this is saying they
are open, it probably means they have been very recently closed and we haven't
updated this.  

If registrations are closed, there will be an explanation as to why we have
decided to close the registrations here as soon as we get it up.  